Discovery server is uit te testen via de volgende url:
Totale film lijst ophalen: http://bramreinold.nl/discoveryserver/movie/list/
Eén film ophalen: http://bramreinold.nl/discoveryserver/movie/list/1/ (1 = id film)
Film toevoegen: http://bramreinold.nl/discoveryserver/movie/list/add/ met json body.

Json film formaat ziet er als volgt uit:
[movieID, movieName, ipAddress, portNumber], voorbeeld:
[{"movieID":0,"movieName":"Voorbeeld film","ipAddress":"192.170.28.4","portNumber":"8000"}]
