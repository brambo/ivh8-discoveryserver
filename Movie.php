<?php
/**
 * Created by PhpStorm.
 * User: bram_
 * Date: 31-5-2016
 * A domain Class to demonstrate RESTful web services
 */
Class Movie {
	
	// Attributes
    // Note: attributes are here public because that is necessary to convert the movie objects in the movie list to json
	public $movieID;
    public $movieName;
    public $ipAddress;
    public $portNumber;

    // Default constructor
    public function __construct($id, $name, $ip, $port) {
        $this->movieID = $id;
        $this->movieName = $name;
        $this->ipAddress = strval($ip);
        $this->portNumber = strval($port);
    }

    // Methods
    public function getPortNumber()
    {
        return $this->portNumber;
    }
    public function setPortNumber($portNumber)
    {
        $this->portNumber = $portNumber;
    }
    public function getMovieID()
    {
        return $this->movieID;
    }
    public function setMovieID($movieID)
    {
        $this->movieID = $movieID;
    }
    public function getMovieName()
    {
        return $this->movieName;
    }
    public function setMovieName($movieName)
    {
        $this->movieName = $movieName;
    }
    public function getIpAddress()
    {
        return $this->ipAddress;
    }
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

}
