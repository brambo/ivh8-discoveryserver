<?php
// Import necessary files
require_once("WebserviceBase.php");
require_once("MovieList.php");
require_once("Movie.php");

/**
 * Created by Jeffrey Oomen & Bram Reinold.
 * IVH8 Discovery server which is used as an API.
 * Date: 31-5-2016
 * Class MovieRestHandler extends from the webservice base class, that is used to create a http header and the status message.
 * This class has all methods which function as the API methods which can be called externally.
 */

class MovieRestHandler extends WebserviceBase {

    /**
     * Will get all movies available in the MovieList class
     * and encode these to json format.
     */
    function getAllMovies() {

        $movieList = new MovieList();
        $rawDataMovies = $movieList->getAllMovies();

        if(empty($rawDataMovies)) { // If no movies are returned
            $statusCode = 404;
            $rawDataMovies = array('error' => 'No movie(s) found!');
        } else {
            $statusCode = 200; // Success code
        }

        $requestContentType = $_SERVER['HTTP_ACCEPT']; // Get the content type
        $this->setHttpHeaders($requestContentType, $statusCode); // Construct headers in the webservicebase class

        // Only return json and when state is 200
        if($statusCode == 200) {
            // Encode the list with movies to JSON format
            $response = $this->encodeJson($rawDataMovies);
            echo $response;
        }
    }

    /**
     * Will get one movie by id and return it as json
     * @param $id the id of a specific movie object
     */
    public function getMovie($id) {

        $movieList = new MovieList();
        $rawDataMovie = $movieList->getMovieByID($id);

        if(empty($rawDataMovie)) {
            // Error
            $statusCode = 404;
            $rawData = array('error' => 'No mobiles found!');       
        } else {
            // Working (OK return)
            $statusCode = 200;
        }

        $requestContentType = $_SERVER['HTTP_ACCEPT'];
        $this ->setHttpHeaders($requestContentType, $statusCode);

        if($requestContentType != "" && $statusCode == 200) {
            $jsonResponse = json_encode($rawDataMovie);
            echo $jsonResponse;
        }
    }

    /**
     * Will pass the json data to the method of MovieList class
     * which will decode the json and write it to a json formatted file.
     */
    public function addMovieToList($jsonMovieObject) {
        $movieList = new MovieList();
        $movieList->addMovieToList($jsonMovieObject);
    }

    public function encodeJson($responseData) {
        $jsonResponse = json_encode($responseData);
        return $jsonResponse;
    }
}
?>