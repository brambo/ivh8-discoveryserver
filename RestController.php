<?php
require_once("MovieRestHandler.php");

/**
 * Created by Jeffrey Oomen & Bram Reinold.
 * IVH8 Discovery server which is used as an API.
 * Date: 31-5-2016
 * Functions as an URI mapper to figure out which API method
 * should be called.
 */
		
$view = "";
if(isset($_GET["view"])) {
	$view = $_GET["view"];
	
    // Controls the RESTful services URL mapping check which type we have to handle
	switch ($view) {
		case "all":
			// Handle REST webservice by ul /movie/list/
			$movieRestHandler = new MovieRestHandler();
			$movieRestHandler->getAllMovies();
			break;
		case "single":
			// Handle REST webservice by ul /movie/list/id/ e.g. /movie/list/1/
			$movieRestHandler = new MovieRestHandler();
			$movieRestHandler->getMovie($_GET["id"]);
			break;
		case "add":
			// Handle REST webservice by ul /movie/list/add/
			$movieRestHandler = new MovieRestHandler();
			$movieRestHandler->addMovieToList(file_get_contents("php://input"));
			break;
		default:
			// 404 do nothing
			break;
	}
}
?>
