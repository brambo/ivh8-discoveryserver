<?php

/**
 * Created by Jeffrey Oomen & Bram Reinold.
 * IVH8 Discovery server which is used as an API.
 * Date: 31-5-2016
 * MovieList class contain all the movies and methods
 * to mutate this movieList.
 */
class MovieList {
    // Attributes
    private $movieList;
    private $decodedJson;

    // Default constructor
    public function __construct() {
        $this->movieList = array(); // will contain all movie objects
        // Will keep track of the movies in php json array format
        // so new json can be added to this array.
        $this->decodedJson = $this->readAllMovies(); 
    }

    // Methods
    /**
     * Will add a movie in php json array format to
     * the .json file and then call the readAllMovies() method
     * to update the movieList variable. 
     */
    public function addMovieToList($jsonMovieObj) {
        // Add new movie to the array
        array_push($this->decodedJson, json_decode($jsonMovieObj));

        $fh = fopen("data.json", 'w') or die("Error opening output file");
        fwrite($fh, json_encode($this->decodedJson)); // Write new json data to file
        fclose($fh);

        //Update the movies list
        $this->decodedJson = $this->readAllMovies();
    }

    /**
     * Will get all movies from the movieList variable.
     */
    public function getAllMovies() {
        return $this->movieList;
    }

    /**
     * Will get the movie from the movieList variable
     * which matches the arguments value.
     */
    public function getMovieByID($id) {
        foreach($this->movieList as $movie) {
            if($id == $movie->movieID) {
                return $movie;
            }
        }
    }

    /**
     * Will read all movies which are stored in a json formatted
     * text file. All data will be automatically converted into
     * Movie objects and added to the movieList variable.
     */
    private function readAllMovies() {
        $json = file_get_contents("data.json"); // Read file contents
        $decodedJson = json_decode($json,true); // Decode file contents (array format)

        // For each json array in the decodedJson variable, make a movie object
        for ($i = 0; $i < count($decodedJson); $i++) {
            $movieElement = $decodedJson[$i];
            array_push($this->movieList, new Movie($i, $movieElement["movieName"], $movieElement["ipAddress"], $movieElement["portNumber"]));
        }
        return $decodedJson;
    }
}